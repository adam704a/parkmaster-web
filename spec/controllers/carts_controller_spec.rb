require File.dirname(__FILE__) + '/../spec_helper'

describe CartsController do
  fixtures :all
  render_views

  it "index action should render index template" do
    get :index
    response.should render_template(:index)
  end

  it "show action should render show template" do
    get :show, :id => Cart.first
    response.should render_template(:show)
  end

  it "new action should render new template" do
    get :new
    response.should render_template(:new)
  end

  it "create action should render new template when model is invalid" do
    Cart.any_instance.stubs(:valid?).returns(false)
    post :create
    response.should render_template(:new)
  end

  it "create action should redirect when model is valid" do
    Cart.any_instance.stubs(:valid?).returns(true)
    post :create
    response.should redirect_to(cart_url(assigns[:cart]))
  end

  it "edit action should render edit template" do
    get :edit, :id => Cart.first
    response.should render_template(:edit)
  end

  it "update action should render edit template when model is invalid" do
    Cart.any_instance.stubs(:valid?).returns(false)
    put :update, :id => Cart.first
    response.should render_template(:edit)
  end

  it "update action should redirect when model is valid" do
    Cart.any_instance.stubs(:valid?).returns(true)
    put :update, :id => Cart.first
    response.should redirect_to(cart_url(assigns[:cart]))
  end

  it "destroy action should destroy model and redirect to index action" do
    cart = Cart.first
    delete :destroy, :id => cart
    response.should redirect_to(carts_url)
    Cart.exists?(cart.id).should be_false
  end
end
