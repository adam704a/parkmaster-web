require 'spec_helper'

describe Account do
  it "should be valid" do
    Account.new.should be_valid
  end
  
  it "should be able to deposit funds" do
    account = Account.new
    account.balance = 10
    account.deposit(100)
    account.balance.should == 110
  end

  it "should be able to withdraw funds" do
    account = Account.new
    account.balance = 100
    account.withdraw(10)
    account.balance.should == 90
  end
  
end


