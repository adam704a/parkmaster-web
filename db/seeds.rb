# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
system = User.create([{email: 'adam704a@yahoo.com', password_salt: '$2a$10$/pCHIiC2055xACcdvd31F.', password_hash: '$2a$10$/pCHIiC2055xACcdvd31F.jBesQsFA9RDP8bxHVzfIG2pUsEnE4rS'}])
