class CreateParkingspots < ActiveRecord::Migration
  def change
    create_table :parkingspots do |t|
      t.datetime :start_time
      t.datetime :end_time
      t.string :location
      t.text :description

      t.timestamps
    end
  end
end
