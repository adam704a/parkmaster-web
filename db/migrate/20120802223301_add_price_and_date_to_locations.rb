class AddPriceAndDateToLocations < ActiveRecord::Migration
  def change
    add_column :locations, :price, :string
    add_column :locations, :opentime, :datetime
    add_column :locations, :closetime, :datetime
  end
end
