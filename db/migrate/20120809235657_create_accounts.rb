class CreateAccounts < ActiveRecord::Migration
  def change
    create_table :accounts do |t|
      t.decimal :balance, :null => false, :default => 0
      t.integer :user_id

      t.timestamps
    end
  end
end
