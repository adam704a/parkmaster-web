class ReplaceUseridWithCartid < ActiveRecord::Migration
  def change
    
    remove_column :payment_notifications, :user_id
    add_column :payment_notifications, :cart_id, :integer
    add_index :payment_notifications, :cart_id
  end

end
