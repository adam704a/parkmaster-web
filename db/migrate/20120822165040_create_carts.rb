class CreateCarts < ActiveRecord::Migration
  def self.up
    create_table :carts do |t|
      t.string :amount
      t.integer  :user_id
      t.datetime :purchased_at
      t.timestamps
    end
    add_index :carts, :user_id
  end

  def self.down
    drop_table :carts
  end
end
