class AddUseridToPaymentnotification < ActiveRecord::Migration
  def change
    add_column :payment_notifications, :user_id, :integer
    add_index :payment_notifications, :user_id
  end
end
