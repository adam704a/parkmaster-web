class AddReservationToLocation < ActiveRecord::Migration
  def change
    add_column :locations, :reservation_id, :integer
    add_index :locations, :reservation_id
  end
end
