ParkmasterWeb::Application.routes.draw do
  
  resources :carts

  resources :payment_notifications

  resources :reservations

  resources :locations
  resources :locations do
    member do
      post 'reserve'
    end
  end

  # The priority is based upon order of creation:
  # first created -> highest priority.

  get "sign_up" => "users#new", :as => "sign_up"  
  root :to => "pages#main"  
  resources :users
  resources :sessions

  
  get "help" => "pages#help", :as => "help"
  get "how_it_works" => "pages#how_it_works", :as => "how_it_works"
    

  get "log_in" => "sessions#new", :as => "log_in" 
  get "log_out" => "sessions#destroy", :as => "log_out"
  get "my_account" => "account#my_account", :as => "my_account"

    
  
end
