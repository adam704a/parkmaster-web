class Reservation < ActiveRecord::Base
  
  attr_accessible :user_id, :location_id
   
  belongs_to :user
  has_one :location
end
