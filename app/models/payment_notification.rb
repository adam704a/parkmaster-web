class PaymentNotification < ActiveRecord::Base
  
  attr_accessible :params, :cart_id, :status, :transaction_id
  serialize :params
  belongs_to :cart
    
  after_create :update_account_balance

  private

  def update_account_balance
    if status == "Completed" && params[:receiver_email] == APP_CONFIG[:paypal_email] && params[:secret] == APP_CONFIG[:paypal_secret]
      
      cart.update_attribute(:purchased_at, Time.now)
      cart.user.account.deposit(params[:mc_gross])
      cart.user.account.save
      
      UserMailer.account_load(cart.user,params[:mc_gross])
    else
      logger.debug "Didn't update account balance for cart: #{cart.id} "
    end
    
  end
  
end
