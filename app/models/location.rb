class Location < ActiveRecord::Base
  attr_accessible :address, :latitude, :longitude, :price, :opentime, :closetime 
  
  belongs_to :user
  belongs_to :reservation
  
  geocoded_by :address
  after_validation :geocode, :if => :address_changed?
  
  
end
