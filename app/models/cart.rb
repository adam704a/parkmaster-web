class Cart < ActiveRecord::Base
  
  validates :amount, :presence => true
  
  belongs_to :user
  attr_accessible :amount, :purchased_at
end
