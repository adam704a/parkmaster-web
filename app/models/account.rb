class Account < ActiveRecord::Base
  attr_accessible :balance, :user_id
  
  belongs_to :user
  
  def withdraw(amount)
    self.balance -= amount.to_d
  end
  
  def deposit(amount)
    self.balance += amount.to_d;
  end
end
