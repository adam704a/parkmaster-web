class UsersController < ApplicationController
  
  layout 'blank'
  
  def new  
      @user = User.new  
    end  

    def create  
      @user = User.new(params[:user])  
      @user.account = Account.new(:user_id => @user.id)
      
      if @user.save  
        UserMailer.welcome_email(@user).deliver
        session[:user_id] = @user.id
        redirect_to root_url 
      else  
        render "new"  
      end  
    end
end
