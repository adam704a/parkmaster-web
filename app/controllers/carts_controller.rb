class CartsController < ApplicationController
  def index
    @carts = Cart.all
  end

  def show
    @cart = Cart.find(params[:id])
  end

  def new
    @cart = Cart.new
  end

  def create
    @cart = Cart.new(params[:cart])
    @cart.user = current_user
    
    if @cart.save
      session[:cart_id] ||= @cart.id
    end

    redirect_to my_account_url
  end

  def edit
    @cart = Cart.find(params[:id])
  end



  def destroy
    @cart = Cart.find(params[:id])
    @cart.destroy
    session[:cart_id] = nil
    redirect_to my_account_url
  end
end
