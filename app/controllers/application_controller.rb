class ApplicationController < ActionController::Base
  protect_from_forgery  
    helper_method :current_user, :current_cart

    private  
    def current_user  
      @current_user ||= User.find(session[:user_id]) if 
        session[:user_id]  
    end
    
    private
    def current_cart

      if session[:cart_id]
        @current_cart ||= Cart.find(session[:cart_id])
        session[:cart_id] = nil if @current_cart.purchased_at
      end
      
      if session[:cart_id].nil?
        @current_cart = Cart.new
      end
      
      @current_cart
    end

end
