class LocationsController < ApplicationController
  
  def index
    @locations = Location.where("opentime > ?", Time.now).where(:reservation_id => nil)
    
    if @locations.length == 0 
       link = "<a href=\"#{url_for(new_location_path)}\">create one now</a>"    
       flash.now[:notice] = "No locations found. Why don't you #{link}?".html_safe
    end
  end

  def show
    @location = Location.find(params[:id])
  end
  
  def reserve
    location = Location.find(params[:id])
    location.reservation = Reservation.new(:user_id => current_user.id,:location_id => location.id)
    
    #make into transaction
    location.user.account.deposit(location.price)
    location.reservation.user.account.withdraw(location.price.to_d)
    
    location.user.account.save
    location.reservation.user.account.save
    
    UserMailer.reservation_confirmation(location.reservation.user, location).deliver
    UserMailer.reservation_notification(location.user, location).deliver
    
    if location.save
      redirect_to locations_path, :notice => "Successfully reserved location"
    else
      redirect_to location, :notice  => "Something bad happend making your reservation"
    end
    
  end

  def find
    
  end

  def new
    @location = Location.new
  end

  def create
    @location = Location.new(params[:location])
    
    logger.debug "Here is the whole thing #{@location} and #{@location.closetime} #{@location.price} "
    
    @location.user = current_user
    
    UserMailer.location_confirmation(current_user, @location).deliver
    
    if @location.save
      redirect_to locations_path
    else
      render :action => 'new'
    end
  end

  def edit
    @location = Location.find(params[:id])
  end

  def update
    @location = Location.find(params[:id])
    if @location.update_attributes(params[:location])
      redirect_to @location, :notice  => "Successfully updated location"
    else
      render :action => 'edit'
    end
  end

  def destroy
    @location = Location.find(params[:id])
    @location.destroy
    redirect_to locations_url
  end
end
