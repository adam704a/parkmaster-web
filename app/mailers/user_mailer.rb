class UserMailer < ActionMailer::Base
  
  include SendGrid
  default from: "admin@parkey.co"
  sendgrid_category :use_subject_lines
  sendgrid_enable   :ganalytics, :opentrack
    
  def welcome_email(user)
      @user = user
      @url  = "http://parkey.co/"
      mail(:to => user.email, :subject => "Welcome to Parkey")
    end
    
  def reservation_notification(user, location)
     @user = user
     mail(:to => user.email, :subject => "Someone has reserved your spot")
  end
  
  def reservation_confirmation(user, location)
   @user = user
   @location = location
   mail(:to => user.email, :subject => "You have reserved a spot")
  end
  
  def location_confirmation(user, location)
     @user = user
     @location = location
     mail(:to => user.email, :subject => "You have listed your spot")
  end
  
  def account_load(user, amount)
    @user = user
    @amount = amount
    mail(:to => user.email, :subject => "Your account has been credited")
  end
    
end
